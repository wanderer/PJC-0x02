#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void print_menu(){
    printf("\n[*] guess a number from 0 to 10\n");
    printf( "[*] press:\n"
            "\t1 to play\n"
            "\t2 to try again\n"
            "\t...or anything else to exit\n\n");
}

int lemmego(){
    printf("[*] you wished to go, quitting...\n");
    return EXIT_SUCCESS;
}

int get_rand(){
    int ar [10];

    for (int i = 0; i < 10; i++) {
        int dat_num = 1+ (rand() % 10);
        ar[i] = dat_num;
    }
    int r = ar[rand() % 10];
    return r;
}

int get_userguess(){
    int x = -1;
    int ok = -1;
    while( ok != 1){
        ok = scanf("%d", &x);
        if(ok == EOF){
            printf("\ncaught Ctrl+D...\n");
            ok = 1;
            x = 2;
            break;
        }
        if(ok == 0){
            while (fgetc(stdin) != '\n'){
            }
            printf("[*] give me a correct value\n");
            printf("[*] stop gibbering around\n    my guess >>> ");
        }
    }
    return x;
}

void read_selection(int selection){
    int ok;
    while(ok != 1) {
        ok = scanf("%d", &selection);
        if(ok == EOF){
            printf("[*] please, come on...\n");
            ok = 1;
            lemmego();
        }
        if(ok == 0){
            while (fgetc(stdin) != '\n'){
            }
            lemmego();
            return;
        }
    }
}

void compare_guess_with_reality(int userguess, int reality){
    if(userguess == reality){
        printf("[*] you won because you've got luck. try me next time!\n");
    }else if(userguess < reality){
        printf("[*] you lost!\n");
        printf("\thint: your guess was just a tad lower than the secret number\n");
    }else{
        printf("[*] you lost!\n");
        printf("[i] hint: your guess was just a tad higher than the secret number\n");
    }
}

void guess(){
    srand(time(0));
    int userchoice = -1;
    int nurand = -1;
    printf("\nwelcome to the guessing game\n");

    while (userchoice != 3){
        int userguess = -1;
        int ok = -1;

        print_menu();

        while(ok != 1){
            printf("[*] you >>> ");
            ok = scanf("%d", &userchoice);
            if(ok == EOF){
                printf("[*] please, come on...\n");
                lemmego();
                break;
            }
            if(ok == 0){
                while (fgetc(stdin) != '\n'){
                }
                lemmego();
                return;
            }
        }

        switch( userchoice ) {
            case 1:
                nurand = get_rand();
                printf( "[?] what is your guess then? pick a number from 0 to 10\n" );
                printf("    my guess >>> ");

                userguess = get_userguess();
                compare_guess_with_reality(userguess, nurand);
                break;

            case 2:
                if(nurand == -1){
                    printf("[!] there wasn't a first time yet, mate...\n");
                    break;
                }
                printf("[*] guess again\n");
                printf( "[?] what is your guess then? pick a number from 0 to 10\n" );
                printf("    my guess >>> ");

                userguess = get_userguess();
                compare_guess_with_reality(userguess, nurand);
                break;

            default:
                userchoice = 3;
                lemmego();
        }
    }
}

void print_rand_bs(){
    printf("\n\n[*] printing rand\n");
    printf("seed\trand1\trand2\trand3\trand4\trand5\trand6\trand7\n");
    for (unsigned int i = 1; i < 6; i++){
        unsigned int seed = i;
        srand(seed);
        printf("%d\t", seed);
        for (int j = 0; j < 7; j++){
            printf("%d\t", rand() % 101);
        }
        printf("\n");
    }
}


void default_flow(char operator, int a, int b, int result){
    int answer = 0;
    printf("a: %d\tb: %d\n", a, b);
    printf("what is %d %c %d?\n>> ", a, operator, b);
    answer = get_userguess();
    if (answer == result){
        printf("splendid. you win %d czk!\n", rand());
    }
    else {
        printf("incorrect, %d %c %d is %d.\n", a, operator, b, result);
    }
}

void another_bs(){
    int a, b, result, answer, selection;
    int upper_limit = 10;

    srand(time(NULL));

    printf("\n\nwelcome to the math game\n");
    while ( selection != 8 ) {
        printf("\npick a number\n");
        printf("1 - addition\n"
               "2 - subtraction\n"
               "3 - multiplication\n"
               "4 - division\n"
               "5 - modulo\n"
               "6 - set upper_limit for x = rand() mod upper_limit\n"
               "7 - XOR\n"
               "8 - quit\n"
               ">> ");

        selection = get_userguess();

        a= 1 + (rand() % upper_limit);
        b = 1 + (rand() % upper_limit);

        switch ( selection ) {
            case 1:
                result = a + b;
                default_flow('+', a, b, result);
                break;
            case 2:
                result = a - b;
                default_flow('-', a, b, result);
                break;
            case 3:
                result = a * b;
                default_flow('*',a , b, result);
                break;
            case 4:
                result = a / b;
                default_flow('/', a, b, result);
                break;
            case 5:
                printf("a: %d\tb: %d\n", a, b);
                result = a % b;
                printf("what is %d mod %d?\n>> ", a, b);
                answer = get_userguess();
                if (answer == result)
                    printf("splendid. you win %d czk!\n", rand());
                else
                    printf("incorrect, %d mod %d je %d.\n", a, b, result);
                break;
            case 6:
                printf("    default limit: 10\n"
                       " *  current limit: %d\n", upper_limit);
                printf("    new limit\t>> ");
                upper_limit = get_userguess();
                printf("\n *  new limit: %d\n", upper_limit);
                break;
            case 7:
                result = a ^ b;
                default_flow('^',a, b, result);
                break;
            case 8:
                lemmego();
                return;
            default:
                printf("invalid option\n");
                selection = 0;
                break;
        }
    }
}

void print_some_multiples_of_five(){
    for (int i = 1; i <= 20; ++i) {
        printf("i = %d\n", i*5);
    }
}

void gimme_leap_years(){
    int former, latter, selection;

    printf("\ncalculate leap years for a given range\n");
    while ( selection != 2 ) {
        printf("\n1 - calculate\n"
               "2 - quit\n"
               ">> ");
        selection = 0;
        selection = get_userguess();

        switch (selection) {
            case 1:
                printf("the former year\n>> ");
                former = get_userguess();
                printf("the latter year\n>> ");

                latter = get_userguess();
                if (former > latter) {
                    int tmp = latter;
                    latter = former;
                    former = tmp;
                }

                for (int i = former; i <= latter; ++i) {
                    int year = i;
                    if (year % 4 == 0) {
                        if (year % 100 == 0) {
                            if (year % 400 == 0) {
                                printf("year %d is a leap year\n", year);
                                continue;
                            } else {
                                printf("year %d is NOT a leap year\n", year);
                                continue;
                            }
                            printf("year %d is NOT a leap year\n", year);
                            continue;
                        }
                        printf("year %d is a leap year\n", year);
                        continue;
                    } else {
                        printf("year %d is NOT a leap year\n", year);
                        continue;
                    }
                }
                printf("range %d..%d (%d years)\n", former, latter, latter - former);
                break;
            case 2:
                lemmego();
                return;
                break;
            default:
                printf("invalid option\n");
                selection = 0;
                break;
        }
    }
}


int main(int argc, char *argv[])
{
    print_rand_bs();
    guess();
    another_bs();
    print_some_multiples_of_five();
    gimme_leap_years();
    return 0;
}
